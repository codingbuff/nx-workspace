import { Component } from '@angular/core';

@Component({
  selector: 'command-dictionary-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'dictionary';
}
