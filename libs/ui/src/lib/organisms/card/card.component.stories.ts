// Button.stories.ts

import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { CardComponent } from './card.component';
export default {
  title: 'Organisms/Card',
  component: CardComponent,
} as Meta;

const Template: Story = (args) => ({
  props: args,
});

// 👇 Each story then reuses that template
export const Default = Template.bind({});
Default.args = {
  label: 'Button',
  primary: true,
};
