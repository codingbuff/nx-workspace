import { Component } from '@angular/core';

@Component({
  selector: 'command-dictionary-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {}
