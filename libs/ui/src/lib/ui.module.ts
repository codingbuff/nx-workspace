import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './organisms/card/card.component';
import { HeaderComponent } from './organisms/header/header.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CardComponent, HeaderComponent],
})
export class UiModule {}
